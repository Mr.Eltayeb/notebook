(function () {
  'use strict';

  angular
    .module('notebooks')
    .controller('NotebooksListController', NotebooksListController);

  NotebooksListController.$inject = ['NotebooksService'];

  function NotebooksListController(NotebooksService) {
    var vm = this;

    vm.notebooks = NotebooksService.query();
  }
}());
