(function () {
  'use strict';

  // Notebooks controller
  angular
    .module('notebooks')
    .controller('NotebooksController', NotebooksController);

  NotebooksController.$inject = ['$scope', '$state', '$window', 'Authentication', 'notebookResolve'];

  function NotebooksController ($scope, $state, $window, Authentication, notebook) {
    var vm = this;

    vm.authentication = Authentication;
    vm.notebook = notebook;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Notebook
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.notebook.$remove($state.go('notebooks.list'));
      }
    }

    // Save Notebook
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.notebookForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.notebook._id) {
        vm.notebook.$update(successCallback, errorCallback);
      } else {
        vm.notebook.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('notebooks.view', {
          notebookId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
