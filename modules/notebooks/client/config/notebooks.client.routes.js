(function () {
  'use strict';

  angular
    .module('notebooks')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('notebooks', {
        abstract: true,
        url: '/notebooks',
        template: '<ui-view/>'
      })
      .state('notebooks.list', {
        url: '',
        templateUrl: 'modules/notebooks/client/views/list-notebooks.client.view.html',
        controller: 'NotebooksListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Notebooks List'
        }
      })
      .state('notebooks.create', {
        url: '/create',
        templateUrl: 'modules/notebooks/client/views/form-notebook.client.view.html',
        controller: 'NotebooksController',
        controllerAs: 'vm',
        resolve: {
          notebookResolve: newNotebook
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Notebooks Create'
        }
      })
      .state('notebooks.edit', {
        url: '/:notebookId/edit',
        templateUrl: 'modules/notebooks/client/views/form-notebook.client.view.html',
        controller: 'NotebooksController',
        controllerAs: 'vm',
        resolve: {
          notebookResolve: getNotebook
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Notebook {{ notebookResolve.name }}'
        }
      })
      .state('notebooks.view', {
        url: '/:notebookId',
        templateUrl: 'modules/notebooks/client/views/view-notebook.client.view.html',
        controller: 'NotebooksController',
        controllerAs: 'vm',
        resolve: {
          notebookResolve: getNotebook
        },
        data: {
          pageTitle: 'Notebook {{ notebookResolve.name }}'
        }
      });
  }

  getNotebook.$inject = ['$stateParams', 'NotebooksService'];

  function getNotebook($stateParams, NotebooksService) {
    return NotebooksService.get({
      notebookId: $stateParams.notebookId
    }).$promise;
  }

  newNotebook.$inject = ['NotebooksService'];

  function newNotebook(NotebooksService) {
    return new NotebooksService();
  }
}());
