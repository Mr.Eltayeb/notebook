(function () {
  'use strict';

  angular
    .module('notebooks')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Notebooks',
      state: 'notebooks',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    // menuService.addSubMenuItem('topbar', 'notebooks', {
    //   title: 'List Notebooks',
    //   state: 'notebooks.list'
    // });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'notebooks', {
      title: 'Create Notebook',
      state: 'notebooks.create',
      roles: ['user']
    });
  }
}());
