// Notebooks service used to communicate Notebooks REST endpoints
(function () {
  'use strict';

  angular
    .module('notebooks')
    .factory('NotebooksService', NotebooksService);

  NotebooksService.$inject = ['$resource'];

  function NotebooksService($resource) {
    return $resource('/api/notebooks/:notebookId', {
      notebookId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
