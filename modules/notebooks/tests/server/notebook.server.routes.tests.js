'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Notebook = mongoose.model('Notebook'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  notebook;

/**
 * Notebook routes tests
 */
describe('Notebook CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Notebook
    user.save(function () {
      notebook = {
        name: 'Notebook name'
      };

      done();
    });
  });

  it('should be able to save a Notebook if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Notebook
        agent.post('/api/notebooks')
          .send(notebook)
          .expect(200)
          .end(function (notebookSaveErr, notebookSaveRes) {
            // Handle Notebook save error
            if (notebookSaveErr) {
              return done(notebookSaveErr);
            }

            // Get a list of Notebooks
            agent.get('/api/notebooks')
              .end(function (notebooksGetErr, notebooksGetRes) {
                // Handle Notebooks save error
                if (notebooksGetErr) {
                  return done(notebooksGetErr);
                }

                // Get Notebooks list
                var notebooks = notebooksGetRes.body;

                // Set assertions
                (notebooks[0].user._id).should.equal(userId);
                (notebooks[0].name).should.match('Notebook name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Notebook if not logged in', function (done) {
    agent.post('/api/notebooks')
      .send(notebook)
      .expect(403)
      .end(function (notebookSaveErr, notebookSaveRes) {
        // Call the assertion callback
        done(notebookSaveErr);
      });
  });

  it('should not be able to save an Notebook if no name is provided', function (done) {
    // Invalidate name field
    notebook.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Notebook
        agent.post('/api/notebooks')
          .send(notebook)
          .expect(400)
          .end(function (notebookSaveErr, notebookSaveRes) {
            // Set message assertion
            (notebookSaveRes.body.message).should.match('Please fill Notebook name');

            // Handle Notebook save error
            done(notebookSaveErr);
          });
      });
  });

  it('should be able to update an Notebook if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Notebook
        agent.post('/api/notebooks')
          .send(notebook)
          .expect(200)
          .end(function (notebookSaveErr, notebookSaveRes) {
            // Handle Notebook save error
            if (notebookSaveErr) {
              return done(notebookSaveErr);
            }

            // Update Notebook name
            notebook.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Notebook
            agent.put('/api/notebooks/' + notebookSaveRes.body._id)
              .send(notebook)
              .expect(200)
              .end(function (notebookUpdateErr, notebookUpdateRes) {
                // Handle Notebook update error
                if (notebookUpdateErr) {
                  return done(notebookUpdateErr);
                }

                // Set assertions
                (notebookUpdateRes.body._id).should.equal(notebookSaveRes.body._id);
                (notebookUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Notebooks if not signed in', function (done) {
    // Create new Notebook model instance
    var notebookObj = new Notebook(notebook);

    // Save the notebook
    notebookObj.save(function () {
      // Request Notebooks
      request(app).get('/api/notebooks')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Notebook if not signed in', function (done) {
    // Create new Notebook model instance
    var notebookObj = new Notebook(notebook);

    // Save the Notebook
    notebookObj.save(function () {
      request(app).get('/api/notebooks/' + notebookObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', notebook.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Notebook with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/notebooks/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Notebook is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Notebook which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Notebook
    request(app).get('/api/notebooks/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Notebook with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Notebook if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Notebook
        agent.post('/api/notebooks')
          .send(notebook)
          .expect(200)
          .end(function (notebookSaveErr, notebookSaveRes) {
            // Handle Notebook save error
            if (notebookSaveErr) {
              return done(notebookSaveErr);
            }

            // Delete an existing Notebook
            agent.delete('/api/notebooks/' + notebookSaveRes.body._id)
              .send(notebook)
              .expect(200)
              .end(function (notebookDeleteErr, notebookDeleteRes) {
                // Handle notebook error error
                if (notebookDeleteErr) {
                  return done(notebookDeleteErr);
                }

                // Set assertions
                (notebookDeleteRes.body._id).should.equal(notebookSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Notebook if not signed in', function (done) {
    // Set Notebook user
    notebook.user = user;

    // Create new Notebook model instance
    var notebookObj = new Notebook(notebook);

    // Save the Notebook
    notebookObj.save(function () {
      // Try deleting Notebook
      request(app).delete('/api/notebooks/' + notebookObj._id)
        .expect(403)
        .end(function (notebookDeleteErr, notebookDeleteRes) {
          // Set message assertion
          (notebookDeleteRes.body.message).should.match('User is not authorized');

          // Handle Notebook error error
          done(notebookDeleteErr);
        });

    });
  });

  it('should be able to get a single Notebook that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Notebook
          agent.post('/api/notebooks')
            .send(notebook)
            .expect(200)
            .end(function (notebookSaveErr, notebookSaveRes) {
              // Handle Notebook save error
              if (notebookSaveErr) {
                return done(notebookSaveErr);
              }

              // Set assertions on new Notebook
              (notebookSaveRes.body.name).should.equal(notebook.name);
              should.exist(notebookSaveRes.body.user);
              should.equal(notebookSaveRes.body.user._id, orphanId);

              // force the Notebook to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Notebook
                    agent.get('/api/notebooks/' + notebookSaveRes.body._id)
                      .expect(200)
                      .end(function (notebookInfoErr, notebookInfoRes) {
                        // Handle Notebook error
                        if (notebookInfoErr) {
                          return done(notebookInfoErr);
                        }

                        // Set assertions
                        (notebookInfoRes.body._id).should.equal(notebookSaveRes.body._id);
                        (notebookInfoRes.body.name).should.equal(notebook.name);
                        should.equal(notebookInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Notebook.remove().exec(done);
    });
  });
});
