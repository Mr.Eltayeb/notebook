'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Notebook = mongoose.model('Notebook'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Notebook
 */
exports.create = function(req, res) {
  var notebook = new Notebook(req.body);
  notebook.user = req.user;

  notebook.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(notebook);
    }
  });
};

/**
 * Show the current Notebook
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var notebook = req.notebook ? req.notebook.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  notebook.isCurrentUserOwner = req.user && notebook.user && notebook.user._id.toString() === req.user._id.toString();

  res.jsonp(notebook);
};

/**
 * Update a Notebook
 */
exports.update = function(req, res) {
  var notebook = req.notebook;

  notebook = _.extend(notebook, req.body);

  notebook.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(notebook);
    }
  });
};

/**
 * Delete an Notebook
 */
exports.delete = function(req, res) {
  var notebook = req.notebook;

  notebook.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(notebook);
    }
  });
};

/**
 * List of Notebooks
 */
exports.list = function(req, res) {
  Notebook.find().sort('-created').populate('user', 'displayName').exec(function(err, notebooks) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(notebooks);
    }
  });
};

/**
 * Notebook middleware
 */
exports.notebookByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Notebook is invalid'
    });
  }

  Notebook.findById(id).populate('user', 'displayName').exec(function (err, notebook) {
    if (err) {
      return next(err);
    } else if (!notebook) {
      return res.status(404).send({
        message: 'No Notebook with that identifier has been found'
      });
    }
    req.notebook = notebook;
    next();
  });
};
