'use strict';

/**
 * Module dependencies
 */
var notebooksPolicy = require('../policies/notebooks.server.policy'),
  notebooks = require('../controllers/notebooks.server.controller');

module.exports = function(app) {
  // Notebooks Routes
  app.route('/api/notebooks').all(notebooksPolicy.isAllowed)
    .get(notebooks.list)
    .post(notebooks.create);

  app.route('/api/notebooks/:notebookId').all(notebooksPolicy.isAllowed)
    .get(notebooks.read)
    .put(notebooks.update)
    .delete(notebooks.delete);

  // Finish by binding the Notebook middleware
  app.param('notebookId', notebooks.notebookByID);
};
